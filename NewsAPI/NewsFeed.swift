//
//  NewsFeed.swift
//  NewsAPI
//
//  Created by XeVoNx on 14/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import Foundation

struct NewsFeed: Codable {
    //check docs to see if some of them will ever be nil/null, then make them optionals
    var status: String = ""
    var totalResults: Int = 0
    var articles:[Article]?
}

struct Article: Codable {
    //If you don't need all properties, ignore some ex: source: {...},
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
}

