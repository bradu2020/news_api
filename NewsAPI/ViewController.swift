//
//  ViewController.swift
//  NewsAPI
//
//  Created by XeVoNx on 14/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let apiKey = "bffdcd2727dd4ff6ba3d391b8652df6d"
        let urlString = "http://newsapi.org/v2/everything?q=bitcoin&from=2020-05-14&sortBy=publishedAt&apiKey=\(apiKey)"
        let url = URL(string: urlString) //may return nil
        
        guard url != nil else { return }
        let session = URLSession.shared
        _ = session.dataTask(with: url!) { (data, resp, err) in
            
            //check for errors and if data is coming back
            if err == nil && data != nil {
                //parse JSON
                let decoder = JSONDecoder()
                do {
                    let newsFeed = try decoder.decode(NewsFeed.self, from: data!)
                    print(newsFeed)
                } catch {
                    print("Err in json parsing")
                }
                
            }
        }.resume()
    }

}

